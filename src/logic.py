import pyopencl as cl
import pyopencl.array
import numpy as np

from abc import ABC, abstractmethod


class Logic(ABC):
    def __init__(self, area_size):
        self.area_size = area_size

    @abstractmethod
    def calculate_next_state(self, current_state):
        return


class OpenCLLogic(Logic):
    def __init__(self, area_size):
        super().__init__(area_size)

        # Obtain an OpenCL platform.
        self.platform = cl.get_platforms()[0]

        # Obtain a device id for at least one device (accelerator).
        self.device = self.platform.get_devices()[0]

        # Create a context for the selected device.
        self.context = cl.Context([self.device])

        # Create and build the accelerator program.
        self.program = cl.Program(self.context, load_text_from_file('kernel')).build()

        # Create a command queue for the target device.
        self.queue = cl.CommandQueue(self.context)

    def calculate_next_state(self, current_state):
        # Allocate device memory and move input data from the host to the device memory.
        mem_flags = cl.mem_flags
        area_size = np.array([len(current_state), len(current_state[0])], np.int32)
        area_size_buf = cl.Buffer(self.context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=area_size)
        matrix_buf = cl.Buffer(self.context, mem_flags.READ_ONLY | mem_flags.COPY_HOST_PTR, hostbuf=current_state)
        result = np.empty((len(current_state) * len(current_state[0])), np.int32)
        destination_buf = cl.Buffer(self.context, mem_flags.WRITE_ONLY, result.nbytes)

        # Associate the arguments to the kernel with kernel object. Deploy the kernel for device execution.
        self.program.matrix_dot_vector(self.queue, np.shape(result), None, area_size_buf, matrix_buf, destination_buf)

        # Move the kernel’s output data to host memory.
        cl.enqueue_copy(self.queue, result, destination_buf)
        result = np.reshape(result, (-1, len(current_state)))

        return result


class CpuLogic(Logic):
    def __init__(self, area_size):
        super().__init__(area_size)

    def calculate_next_state(self, current_state):
        next_state = np.zeros((self.area_size[0], self.area_size[1]), np.int32)

        for x in range(self.area_size[0]):
            for y in range(self.area_size[1]):
                next_state[x][y] = self.next_cell_state(current_state, x, y)

        return next_state

    def next_cell_state(self, current_state, cell_pos_x, cell_pos_y):
        alive_neighbours = 0

        for x in range(cell_pos_x - 1, cell_pos_x + 2):
            for y in range(cell_pos_y - 1, cell_pos_y + 2):
                if x < 0 or x >= self.area_size[0] or y < 0 or y >= self.area_size[1]:  # Cell out of the simulation area, TODO
                    continue

                if x == cell_pos_x and y == cell_pos_y:
                    continue

                alive_neighbours += current_state[x][y]

        if current_state[cell_pos_x][cell_pos_y] == 0:
            if alive_neighbours == 3:
                return True

        if current_state[cell_pos_x][cell_pos_y] == 1:
            if alive_neighbours == 2 or alive_neighbours == 3:
                return True

        return False


def load_text_from_file(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()

    return ''.join(map(str, lines))

