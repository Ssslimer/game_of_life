import threading
import numpy as np
import time

exitFlag = 0


def current_ms_time():
    return int(round(time.time_ns()/1000000))


class LogicThread(threading.Thread):
    iteration = 0
    is_running = True
    is_paused = False

    def __init__(self, name, game, target_tps, logic):
        threading.Thread.__init__(self)

        self.name = name
        self.game = game
        self.logic = logic
        if target_tps == -1:
            self.tick_time = 0
        else:
            self.tick_time = 1000 / target_tps

    def run(self):
        while self.is_running:
            if self.is_paused:
                time.sleep(self.tick_time/1000)
                continue

            time_before = current_ms_time()
            self.tick()
            if self.tick_time == 0:
                continue
            delta_time = current_ms_time() - time_before
            print("Tick time: " + str(delta_time) + "ms")
            sleep_time = self.tick_time - delta_time

            if sleep_time > 0:
                time.sleep(sleep_time/1000)

    def tick(self):
        print("TICK: " + str(self.iteration))
        self.iteration += 1

        current_state = np.empty_like(self.game.current_state)
        current_state[:] = self.game.current_state

        self.game.current_state = self.logic.calculate_next_state(current_state)

    def change_tps(self, new_target_tps):
        self.tick_time = 1000 / new_target_tps
