import pygame
import pygame.freetype
import numpy as np
import time


from src.button import Button
from src.logic_thread import LogicThread
from src.logic import CpuLogic, OpenCLLogic

GRAY = (200, 200, 200)
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
RED = (255, 0, 0)

FONT_SIZE = 30


def current_ms_time():
    return int(round(time.time_ns()/1000000))


class Game:
    is_running = True
    ui_elements = list()
    button_pause = None
    button_resume = None
    button_decrease_tps = None
    button_increase_tps = None
    edited_cells = list()

    def __init__(self, window_width, window_height, area_width, area_height, target_tps, logic):
        self.window_width = window_width
        self.window_height = window_height
        self.area_width = area_width
        self.area_height = area_height
        self.target_tps = target_tps

        self.cell_width = window_width / area_width
        self.cell_height = window_height / area_height
        self.current_state = np.zeros((area_width, area_height), dtype=np.int32)  # 0 if cell is dead, 1 if alive

        pygame.init()
        self.game_display = pygame.display.set_mode((window_width, window_height))
        pygame.display.set_caption('Game of Life')
        self.clock = pygame.time.Clock()
        self.font = pygame.freetype.SysFont("Courier", FONT_SIZE, bold=True)

        self.current_state[5][4] = 1
        self.current_state[5][5] = 1
        self.current_state[5][6] = 1

        self.logic_thread = LogicThread("LogicThread", self, target_tps=target_tps, logic=logic)
        self.logic_thread.start()

        self.init_buttons()

    def init_buttons(self):
        self.button_pause = Button(
            center_position=(50, self.window_height-50),
            font_size=FONT_SIZE,
            bg_rgb=GRAY,
            text_rgb=WHITE,
            text="Pauze",
            font=self.font
        )
        self.ui_elements.append(self.button_pause)

        self.button_resume = Button(
            center_position=(200, self.window_height-50),
            font_size=FONT_SIZE,
            bg_rgb=GRAY,
            text_rgb=WHITE,
            text="Resume",
            font=self.font
        )
        self.ui_elements.append(self.button_resume)

        self.button_decrease_tps = Button(
            center_position=(350, self.window_height-50),
            font_size=FONT_SIZE,
            bg_rgb=GRAY,
            text_rgb=WHITE,
            text="Slower",
            font=self.font
        )
        self.ui_elements.append(self.button_decrease_tps)

        self.button_increase_tps = Button(
            center_position=(500, self.window_height-50),
            font_size=FONT_SIZE,
            bg_rgb=GRAY,
            text_rgb=WHITE,
            text="Faster",
            font=self.font
        )
        self.ui_elements.append(self.button_increase_tps)

    def loop(self):
        mouse_lb_clicked = False
        mouse_lb_down = False

        while self.is_running:
            if mouse_lb_clicked:  # reset, as the click should last 1 frame only
                mouse_lb_clicked = False

            for event in pygame.event.get():
                if event.type == pygame.MOUSEBUTTONDOWN and event.button == 1:
                    mouse_lb_clicked = True
                    mouse_lb_down = True
                if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
                    mouse_lb_down = False

                if event.type == pygame.QUIT:
                    self.is_running = False
                    self.logic_thread.is_running = False
                    self.edited_cells.clear()

            if self.logic_thread.is_paused and mouse_lb_down:
                mouse_pos = pygame.mouse.get_pos()
                cell_x = int(float(mouse_pos[0]) / self.window_width * self.area_width)
                cell_y = int(float(mouse_pos[1]) / self.window_height * self.area_height)
                pos = (cell_x, cell_y)

                if self.edited_cells.__contains__(pos):
                    continue

                self.edited_cells.append((cell_x, cell_y))

                if self.current_state[cell_x][cell_y] == 1:
                    self.current_state[cell_x][cell_y] = 0
                else:
                    self.current_state[cell_x][cell_y] = 1

            self.update_screen(mouse_lb_clicked)

    def update_screen(self, mouse_pressed):
        self.game_display.fill(BLACK)

        for x in range(self.area_width):
            for y in range(self.area_height):
                if self.current_state[x, y] == 1:
                    self.draw_cell(x, y)

        for element in self.ui_elements:
            element.update(pygame.mouse.get_pos(), mouse_pressed)
            element.draw(self.game_display)

        if self.button_pause.is_pressed:
            self.logic_thread.is_paused = True

        if self.button_resume.is_pressed:
            self.logic_thread.is_paused = False

        if self.button_decrease_tps.is_pressed:
            self.target_tps -= 1
            if self.target_tps < 1:
                self.target_tps = 1
            self.logic_thread.change_tps(self.target_tps)

        if self.button_increase_tps.is_pressed:
            self.target_tps += 1
            self.logic_thread.change_tps(self.target_tps)

        tps_text, _ = self.font.render('TPS: ' + str(self.target_tps), bgcolor=GRAY, fgcolor=WHITE, size=FONT_SIZE)
        self.game_display.blit(tps_text, (0, 0))

        pygame.display.update()
        self.clock.tick(60)

    def draw_cell(self, pos_x, pos_y):
        window_pos_x = pos_x * self.cell_width
        window_pos_y = pos_y * self.cell_height
        filled_rect = pygame.Rect(window_pos_x, window_pos_y, self.cell_width, self.cell_height)
        pygame.draw.rect(self.game_display, WHITE, filled_rect)


WINDOW_SIZE = (1000, 1000)
AREA_SIZE = (100, 100)
TARGET_TPS = 5


def main():
    game = Game(window_width=WINDOW_SIZE[0], window_height=WINDOW_SIZE[1], area_width=AREA_SIZE[0], area_height=AREA_SIZE[1], target_tps=TARGET_TPS,
                logic=OpenCLLogic(area_size=(AREA_SIZE[0], AREA_SIZE[1])))
    game.loop()
    stop()


def stop():
    pygame.quit()
    quit()


main()
