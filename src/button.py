from pygame.sprite import Sprite


class Button(Sprite):
    def __init__(self, center_position, text, font_size, bg_rgb, text_rgb, font):
        self.mouse_over = False
        self.is_pressed = False

        default_image = create_surface_with_text(font=font, text=text, font_size=font_size, text_rgb=text_rgb, bg_rgb=bg_rgb)
        highlighted_image = create_surface_with_text(font=font, text=text, font_size=font_size * 1.2, text_rgb=text_rgb, bg_rgb=bg_rgb)

        self.images = [default_image, highlighted_image]
        self.rects = [
            default_image.get_rect(center=center_position),
            highlighted_image.get_rect(center=center_position),
        ]

        super().__init__()

    @property
    def image(self):
        return self.images[1] if self.mouse_over else self.images[0]

    @property
    def rect(self):
        return self.rects[1] if self.mouse_over else self.rects[0]

    def update(self, mouse_pos, mouse_pressed):
        if self.rect.collidepoint(mouse_pos):
            self.mouse_over = True
            self.is_pressed = mouse_pressed
        else:
            self.mouse_over = False
            self.is_pressed = False

    def draw(self, surface):
        surface.blit(self.image, self.rect)


def create_surface_with_text(font, text, text_rgb, bg_rgb, font_size):
    surface, _ = font.render(text=text, fgcolor=text_rgb, bgcolor=bg_rgb, size=font_size)
    return surface.convert_alpha()
