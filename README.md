**Conway's Game of Life**
==========
This is a small project. Its main objective was to check how does the game behave. Besides this I wanted to use OpenCL for the first time.

For detailed description of the game mechanics check the Wikipedia website:
https://en.wikipedia.org/wiki/Conway's_Game_of_Life

-----------
**Dependencies**:

* NumPy
* Pygame
* PyOpenCL

-----------
**Instalation**:
I suggest using some virtual environments to store project and dependencies. I have used python venv.
Instalation of NumPy and Pygame is straightforward. 
PyOpenCL is much more difficult to set up. Obviously you need a GPU to run the program. Either discrete or integrated.
Depending on the vendor of your GPU, you need to install OpenCL SDK and than PyOpenCL. I suggest building it from source files
